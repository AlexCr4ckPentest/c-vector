#ifndef __C_VECTOR_H__
#define __C_VECTOR_H__

#define __C_VECTOR__MAJOR_VERSION 0
#define __C_VECTOR__MINOR_VERSION 2
#define __C_VECTOR__PATCH_VERSION 3

#include <stdlib.h>
#include <stdint.h>

typedef struct vector
{
    void** buffer_;
    size_t size_;
    size_t capacity_;
    // deleter for object in buffer_ memory
    // this requires when dynamicly allocated objects are storing in vector
    void(*deleter_)(void*);
} vector_t;

typedef void** vector_iterator_t;
typedef void** vector_reverse_iterator_t;



static inline vector_iterator_t vector__begin(vector_t* vector)
{ return &vector->buffer_[0]; }

static inline vector_iterator_t vector__end(vector_t* vector)
{ return &vector->buffer_[vector->size_]; }

static inline vector_reverse_iterator_t vector__reverse_begin(vector_t* vector)
{ return &vector->buffer_[vector->size_ - 1]; }

static inline vector_reverse_iterator_t vector__reverse_end(vector_t* vector)
{ return &vector->buffer_[-1]; }

static inline void vector__iterator__next(vector_iterator_t* iter)
{ (*iter)++; }

static inline void vector__reverse_iterator__next(vector_reverse_iterator_t* iter)
{ (*iter)--; }

static inline void vector__iterator__advance(vector_iterator_t* iter, size_t n)
{ (*iter) += n; }

static inline void vector__reverse_iterator__advance(vector_reverse_iterator_t* iter, size_t n)
{ (*iter) -= n; }



static inline void __vector__allocate_buffer(void*** vector_buff, size_t size)
{ *vector_buff = malloc(size); }

static inline void __vector_reallocate_buffer(void*** vector_buff, size_t new_size)
{ *vector_buff = realloc(*vector_buff, new_size); }

static inline size_t vector__capacity(vector_t* vector)
{ return vector->capacity_ / sizeof(void*); }

static inline size_t vector__size(vector_t* vector)
{ return vector->size_; }

// WARNING!!! UNSAFE!!!
// returns pointer to buffer
static inline void*** vector__get_buffer_ptr(vector_t* vector)
{ return &vector->buffer_; }

static inline void* vector__set_custom_deleter(vector_t* vector, void(*deleter)(void* vector_elem))
{
    void(*old_deleter)(void*) = vector->deleter_;
    vector->deleter_ = deleter;
    return old_deleter;
}

static inline vector_t vector__default_constructor()
{
    vector_t empty_vector = {
        .buffer_ = NULL,
        .size_ = 0,
        .capacity_ = 0,
        .deleter_ = NULL
    };
    return empty_vector;
}

vector_t vector__constructor(size_t elements);
void vector__destructor(vector_t* vector);
void vector__push_back(vector_t* vector, void* data);

static inline void vector__pop_back(vector_t* vector)
{
    if (!vector->buffer_) { return; }
    vector->size_--;
}

static inline void* vector__at(vector_t* vector, size_t index)
{
    if (!vector->buffer_ || index >= vector->size_) { return NULL; }
    return vector->buffer_[index];
}

static inline void* vector__front(vector_t* vector)
{
    if (!vector->buffer_) { return NULL; }
    return vector->buffer_[0];
}

static inline void* vector__back(vector_t* vector)
{
    if (!vector->buffer_) { return NULL; }
    return vector->buffer_[vector->size_ - 1];
}

void vector__copy(vector_t* dst_vector, vector_t* src_vector);
void vector__move(vector_t* dst_vector, vector_t* src_vector);

// TODO: implement binary search
int __vector__find_impl_binsearch(vector_t* vector, void* data, size_t data_size, size_t left, size_t right);

int __vector__find_impl(vector_t* vector, void* data, size_t data_size);
int vector__find_if(vector_t* vector, int(*predicate)(void* data));



#define __vector__debug__print(vector, value_type, fmt_string) \
    {   \
        if (!(vector)->buffer_) { printf("<Empty>"); }  \
        else { for (size_t i = 0; i < (vector)->size_; i++) { printf(fmt_string, (value_type)(vector)->buffer_[i]); } }    \
    }

#define vector__find(vector, data, data_type)  \
    __vector__find_impl(vector, data, sizeof(data_type))

#endif // __VECTOR_H__
