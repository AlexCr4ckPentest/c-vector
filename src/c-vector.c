#include <memory.h>

#include "c-vector.h"

enum { VECTOR_OPTIMIZE_ALLOCS = sizeof(void*) * 3 };



vector_t vector__constructor(size_t elements)
{
    vector_t vector = {
        .size_ = 0,
        .capacity_ = VECTOR_OPTIMIZE_ALLOCS + sizeof(void*) * elements,
        .deleter_ = NULL
    };

    __vector__allocate_buffer(&vector.buffer_, vector.capacity_);

    return vector;
}

void vector__destructor(vector_t* vector)
{
    // clear all dynamicly allocated elements
    if (vector->deleter_)
    {
        for (size_t i = 0; i < vector->size_; i++)
        {
            vector->deleter_(vector->buffer_[i]);
        }
    }

    free(vector->buffer_);
    vector->buffer_ = NULL;
    vector->size_ = 0;
    vector->capacity_ = 0;
}

void vector__push_back(vector_t* vector, void* data)
{
    if (!vector->buffer_)
    {
        vector->capacity_ = VECTOR_OPTIMIZE_ALLOCS;
        __vector__allocate_buffer(&vector->buffer_, vector->capacity_);
    }
    else
    {
        if ((vector->size_ * sizeof(void*)) >= vector->capacity_)
        {
            vector->capacity_ += VECTOR_OPTIMIZE_ALLOCS;
            __vector_reallocate_buffer(&vector->buffer_, vector->capacity_);
        }
    }

    vector->buffer_[vector->size_] = data;
    vector->size_++;
}

void vector__copy(vector_t* dst_vector, vector_t* src_vector)
{
    // Prevent self-copy
    if (dst_vector == src_vector)
    {
        return;
    }

    if (!src_vector->buffer_)
    {
        *dst_vector = vector__default_constructor();
    }
    else
    {
        void** old_dst_buffer = dst_vector->buffer_;
        void** new_dst_bufer = NULL;

        __vector__allocate_buffer(&new_dst_bufer, src_vector->capacity_);
        memcpy(new_dst_bufer, src_vector->buffer_, src_vector->capacity_);

        free(old_dst_buffer);
        old_dst_buffer = NULL;

        dst_vector->buffer_ = new_dst_bufer;
        dst_vector->size_ = src_vector->size_;
        dst_vector->capacity_ = src_vector->capacity_;
        dst_vector->deleter_ = src_vector->deleter_;
    }
}

void vector__move(vector_t* dst_vector, vector_t* src_vector)
{
    // Prevent self-copy
    if (dst_vector == src_vector)
    {
        return;
    }

    free(dst_vector->buffer_);

    memcpy(dst_vector, src_vector, sizeof(vector_t));
    memset(src_vector, 0, sizeof(vector_t));
}

int __vector__find_impl_binsearch(vector_t* vector, void* data, size_t data_size, size_t left, size_t right)
{
    // TODO: implement binary search
    return -1;
}

int __vector__find_impl(vector_t* vector, void* data, size_t data_size)
{
    const size_t vector_size = vector->size_;
    for (size_t i = 0; i < vector_size; i++)
    {
        if (memcmp(&vector->buffer_[i], &data, data_size) == 0)
        {
            return i;
        }
    }
    return -1;
}

int vector__find_if(vector_t* vector, int(*predicate)(void* data))
{
    const size_t vector_size = vector->size_;
    for (size_t i = 0; i < vector_size; i++)
    {
        if (predicate(vector->buffer_[i]))
        {
            return i;
        }
    }
    return -1;
}
